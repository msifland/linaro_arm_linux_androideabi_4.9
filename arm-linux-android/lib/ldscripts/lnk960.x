/* Default linker script, for normal executables */
SECTIONS
{
    .text :
    {

	*(.text)
	 _etext = .;

    }
    .data :
    {
 	*(.data)
	CONSTRUCTORS
	 _edata = .;
    }
    .bss :
    {
	 _bss_start = .;
	*(.bss)
	*(COMMON)
	 _end = .;
    }
}
