/* Default linker script, for normal executables */
SECTIONS
{
    .text :
    {
	 CREATE_OBJECT_SYMBOLS
	*(.text)
	 _etext = .;

    }
    .data :
    {
 	*(.data)
	CONSTRUCTORS
	 _edata = .;
    }
    .bss :
    {
	 _bss_start = .;
	*(.bss)
	*(COMMON)
	 _end = .;
    }
}
