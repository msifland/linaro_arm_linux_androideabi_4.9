/* Default linker script, for normal executables */
/* Create a cp/m executable; load and execute at 0x100.  */
OUTPUT_FORMAT("binary")
. = 0x100;
__Ltext = .;
ENTRY (__Ltext)
OUTPUT_ARCH("z80")
SECTIONS
{
.text :	{
	*(.text)
	*(text)
	 __Htext = .;
	}
.data :	{
	 __Ldata = .;
	*(.data)
	*(data)
	 __Hdata = .;
	}
.bss :	{
	 __Lbss = .;
	*(.bss)
	*(bss)
	 __Hbss = .;
	}
}
